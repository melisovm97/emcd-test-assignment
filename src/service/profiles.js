/**
 *  @description Fetches profiles from API
 *  @param { number } page
 *  @param { number } items
 *  @returns { Promise<{ results: { name: String, email: String, picture: String}[] }> }
 */
export const fetchProfiles = async (page = 1, items = 12) => {
  /**
   *
   * @type {Response}
   */
  const response = await fetch(
    `https://randomuser.me/api/?page=${page}&results=${items}`,
  );

  /**
   * @description Convert Response type to normal object
   * @type {{ results: {name: { title: String, first: String, last: String }, email: String, picture: { large: String }}[] }}
   */
  const data = await response.json();

  if (data.results && data.results.length) {
    return {
      results: data.results.map(({ name, email, picture }) => ({
        name: `${name.title} ${name.first} ${name.last}`,
        email,
        picture: picture.large,
      })),
    };
  }
};
