import { fetchProfiles } from '@/service/profiles';

export const ProfileService = { fetchProfiles };
