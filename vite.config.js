import vue from '@vitejs/plugin-vue';
import { fileURLToPath, URL } from 'url';

export default {
  plugins: [vue()],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url)),
    },
  },
  server: {
    port: 8008,
    host: true,
  },
  build: {
    target: 'es2015',
  },
};
