module.exports = {
    extends: [
        'eslint:recommended',
        'plugin:vue/vue3-recommended',
        'prettier'
    ],
    rules: {
        'vue/no-unused-vars': 'error'
    }
}
